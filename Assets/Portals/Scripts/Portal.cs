using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Rendering.Universal;

namespace Rpg.Environment.Portals
{
    public class Portal : MonoBehaviour
    {
        [SerializeField] Portal linkedPortal;
        [SerializeField] MeshRenderer screen;

        Camera playerCamera;
        Camera portalCamera;

        RenderTexture viewTexture;
        static readonly int MainTexture = Shader.PropertyToID("_BaseMap");

        int counter;
        
        void OnEnable()
        {
            playerCamera = Camera.main;

            portalCamera = GetComponentInChildren<Camera>();
            portalCamera.enabled = false;

            counter = PortalCamera.RegisterPortal(this);
        }
		
		void OnDisable() 
		{			            
            portalCamera.enabled = true;
            counter = PortalCamera.UnregisterPortal(this);
		}

        public void PreRender(ScriptableRenderContext context) { }

        public void Render(ScriptableRenderContext context)
        {
            if (linkedPortal == null || !VisibleFromCamera(linkedPortal.screen, playerCamera)) return;

            screen.enabled = false; // disable for calculations
            CreateViewTexture();

            // Apply player cam's transform to the portal cam's
            Matrix4x4 m = transform.localToWorldMatrix
                        * linkedPortal.transform.worldToLocalMatrix
                        * playerCamera.transform.localToWorldMatrix;
            portalCamera.transform.SetPositionAndRotation(m.GetColumn(3), m.rotation);
                        
            UniversalRenderPipeline.RenderSingleCamera(context, portalCamera);
            screen.enabled = true;
        }

        public void PostRender(ScriptableRenderContext context) { }

        void OnValidate()
        {
            if (linkedPortal != null) linkedPortal.linkedPortal = this; // Paired portals
        }

        RenderTexture CreateViewTexture()
        {
            if (viewTexture == null || HasBadViewDimensions())
            {
                if (viewTexture != null) viewTexture.Release();
                viewTexture = new RenderTexture(Screen.width, Screen.height, 0);
                portalCamera.targetTexture = viewTexture; // the view from the portal  				
                linkedPortal.screen.material.SetTexture(MainTexture, viewTexture); // display it
            }
            return viewTexture;

            bool HasBadViewDimensions() => viewTexture.width != Screen.width
                                        || viewTexture.height != Screen.height;
        }
		
        #if UNITY_EDITOR
        [ContextMenu("Save RenderTexture")]
        void SaveRenderTexture()
        {
            RenderTexture render = CreateViewTexture();

            var texture = new Texture2D(Screen.width, Screen.height, TextureFormat.RGB24, false);
            RenderTexture.active = render;
            texture.ReadPixels(new Rect(0, 0, texture.width, texture.height), 0, 0);
            texture.Apply();
            byte[] bytes = texture.EncodeToPNG();
            System.IO.File.WriteAllBytes(System.IO.Path.Combine(
            Application.dataPath, "Rpg", "Levels", "_Test", "Portals", $"Portal-{counter}.png"
            ), bytes);
        }
        #endif

        static bool VisibleFromCamera(Renderer renderer, Camera camera)
        {
            Plane[] frustrumPlanes = GeometryUtility.CalculateFrustumPlanes(camera);
            return GeometryUtility.TestPlanesAABB(frustrumPlanes, renderer.bounds); // is intersecting?
        }
		
		void OnDrawGizmos() 
		{
			if (Application.isPlaying) 
			{
				Gizmos.color = counter == 1 ? Color.blue : (counter == 2 ? Color.red : Color.yellow);
				Gizmos.DrawWireSphere(portalCamera.transform.position, 0.5f);
			}
		}
    }
}
