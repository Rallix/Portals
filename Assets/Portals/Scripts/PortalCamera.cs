﻿using System;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.Rendering;

namespace Rpg.Environment.Portals
{
    public class PortalCamera : MonoBehaviour
    {
        static List<Portal> portals = new List<Portal>();
		
		void OnEnable() 
		{
			RenderPipelineManager.beginCameraRendering += RenderPortal;
		}

		void OnDisable()
		{
			RenderPipelineManager.beginCameraRendering -= RenderPortal;
		}

		static void RenderPortal(ScriptableRenderContext context, Camera camera) // OnPreCull
		{
			// Do all render stages sequentially
			foreach (Portal portal in portals) portal.PreRender(context);
			foreach (Portal portal in portals) portal.Render(context);
			foreach (Portal portal in portals) portal.PostRender(context);
		}

		public static int RegisterPortal(Portal portal) 
		{
			if (portal != null && !portals.Contains(portal)) portals.Add(portal);
			return portals.Count;
		}
		
		public static int UnregisterPortal(Portal portal) 
		{
			if (portal != null && !portals.Contains(portal)) portals.Remove(portal);
			return portals.Count;
		}
		
    }
}