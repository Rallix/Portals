﻿Shader "Rpg/Portal"
{
    Properties
    {
        _InactiveColour ("Inactive Colour", Color) = (1, 1, 1, 1)
        [MainTexture] _BaseMap("Base Map", 2D) = "white"
    }
    SubShader
    {
        Tags
        {
            "RenderType" = "Opaque"
            "RenderPipeline" = "UniversalPipeline"
        }

        LOD 100 // Same as unlit materials
        Cull Off // Two-sided geometry

        Pass
        {
            HLSLPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"

            struct Attributes
            {
                float4 vertex : POSITION; // position OS
                float2 uv : TEXCOORD0;
            };

            struct Varyings
            {
                float4 vertex : SV_POSITION; // position HCS
                float4 uv : TEXCOORD0; // screen
            };

            TEXTURE2D(_BaseMap);
            SAMPLER(sampler_BaseMap);

            float4 _InactiveColour;
            int displayMask; // set to 1 to display texture, otherwise will draw test colour


            float4 ObjectToClipPos(float3 pos) // 'UnityObjectToClipPos()'
            {
                return mul(UNITY_MATRIX_VP, mul(UNITY_MATRIX_M, float4(pos, 1)));
            }

            CBUFFER_START(UnityPerMaterial)
            // The following line declares the _BaseMap_ST variable, so that you
            // can use the _BaseMap variable in the fragment shader. The _ST
            // suffix is necessary for the tiling and offset function to work.
            float4 _BaseMap_ST;
            CBUFFER_END

            Varyings vert(Attributes IN)
            {
                Varyings OUT;
                
                VertexPositionInputs vertexInputs = GetVertexPositionInputs(IN.vertex.xyz);
                OUT.vertex = vertexInputs.positionCS; // UnityObjectToClipPos - TransformObjectToHClip(IN.vertex.xyz)
                
                OUT.uv = ComputeScreenPos(OUT.vertex); // ComputeScreenPos - TRANSFORM_TEX(IN.uv, _BaseMap)
                return OUT;
            }

            half4 frag(Varyings IN) : SV_Target
            {
                float2 screenUV = IN.uv.xy / IN.uv.w;
                half4 portalColor = SAMPLE_TEXTURE2D(_BaseMap, sampler_BaseMap, screenUV);
                return portalColor;// * displayMask + _InactiveColour * (1 - displayMask);
            }
            ENDHLSL
        }
    }
    Fallback "Universal Render Pipeline/Lit" // for shadows
}